# README #

This script was created for PCI Compliance in a clients environment.

Uses SAM (Serverless Application Model) and runs in Lambda with Reports going to CloudWatch

This will be easily adapted for SNS and SES if someone has time to change / update

### Compliance Checks ###

* IAM Access Keys Age
* IAM Password Age (Report Only)
* GIT/CodeCommit HTTPS Access
* GIT/CodeCommit SSH Access
* Unused WorkSpaces Check

### How do I get set up? ###

This solution can be complied with Docker (Recomended) or Python 3.8

##### Build with Docker #####

> call pip install -r SAM/requirements.txt

> call sam build AwsCredentialAgeFunction --use-container

##### Build with Python 3.8 #####

DO NOT use Python 3.9, ensure PATH is set for 3.8 of this will fail (Best option is use Docker)

> call pip install -r SAM/requirements.txt

> call sam build AwsCredentialAgeFunction

##### Upload to AWS #####

Before uploading review your account in samconfig.toml

> call sam deploy --profile demo-sandbox --config-env demo-sandbox --stack-name demo-iamcredentialmanager --role-arn arn:aws:iam::123456:role/CloudFormationDemo

### Who do I talk to? ###

* Sorry, the Author no longer works for Datacom => https://github.com/GrumpyBum
* Solution driven by an internal Architect => Tim Wild, timwi@datacom.co.nz
* Refure to the Datacom CPS Team in Wellington for Python and AWS SAM Support